=======================================================================================================================
=======================================================================================================================
/*
** m0nk Notes on the Sony Xperia Z regulation framework.  This is a highly modified version of the 
**		board-sony_yuga-regulator.c
** file (for readability) and full of notes.
***********************************************************************************************************************
** In general, first you need to glance between this file, the defconfig (fusion3_yuga_defconfig) and a terminal 
**	grepping the source for "names"
**
** 			m0nk@lucy-m0nk-linux:~/aokp_jb/kernel/sony/apq8064$ pwd
**			/home/m0nk/aokp_jb/kernel/sony/apq8064
**			m0nk@lucy-m0nk-linux:~/aokp_jb/kernel/sony/apq8064$ grep -ri msm_sdcc
**
**
***********************************************************************************************************************
** For modifying the voltages to the main SNAPDRAGON cores you need to modify:
**		{ S5, S6, 8821_S0, 8821_S1 } and then modify the Thermal Framework checks and the clocking files
***********************************************************************************************************************
** For modifying the voltages to the NAND and SD Cards you need to modify:
**		{ L5, L6, L7, S4 }- This should modify the voltages into the Qualcomm MSM 7X00A SDCC (see inline notes) / see other notes
***********************************************************************************************************************
** For modifying the voltages to the USB Hardware you need to modify:
**		{L3, L23, S3}
**
** For modifying the voltages to the USB OTG Hardware you need to modify:
**		{L3, L4, S3}
**
** For modifying the voltages to the WiFi chipset you need to modify:
**		{L4, L10, L24, S2, S3, LVS1, LVS2}
**
** For modifying the voltages to the _____ chipset you need to modify:
**		{}
**
** For modifying the voltages to the _____ chipset you need to modify:
**		{}
**
*/
=======================================================================================================================
=======================================================================================================================
Power Regulator Supplies

// **** Normal Toys **** //
//ID 	a_on 	pd 		ss 		min_uV   	max_uV  	supply 		sys_uA  	freq  	fm  	ss_fm 	init_ip	

S1 		1 		1 		0 		1225000 	1225000 	NULL 		100000 		3p20 	NONE 	NONE
S2 		0 		1 		0 		1300000 	1300000 	NULL   		0 			1p60 	NONE 	NONE
S3 		0 		1 		1 		500000 		1150000 	NULL 		100000 		4p80 	NONE 	NONE
S4 		1 		1 		0 		1800000 	1800000 	NULL 		100000 		1p60 	NONE 	NONE
S7 		0 		0 		0 		1300000 	1300000 	NULL 		100000 		3p20 	NONE 	NONE
L1 		1 		1 		0 		1100000 	1100000 	"8921_s4"  	0 									1000
L2 		0 		1 		0 		1200000 	1200000 	"8921_s4"  	0     								0
L3 		0 		1 		0 		3075000 	3075000 	NULL       	0     								0
L4 		1 		1 		0 		1800000 	1800000 	NULL       	0 									10000
L5 		0 		1 		0 		2950000 	2950000 	NULL       	0     								0
L6 		0 		1 		0 		2950000 	2950000 	NULL       	0     								0
L7 		0 		1 		0 		1850000 	2950000 	NULL       	0     								0
L8 		0 		1 		0 		2800000 	2800000 	NULL       	0     								0
L9 		0 		1 		0 		2850000 	2850000 	NULL       	0     								0
L10		0 		1 		0 		2900000 	2900000 	NULL       	0     								0
L11		0 		1 		0 		2850000 	2850000 	NULL       	0     								0
L12		0 		1 		0 		1200000 	1200000 	"8921_s4"  	0     								0
L13		0 		0 		0 		1740000 	1740000 	NULL       	0     								0
L14		0 		1 		0 		1800000 	1800000 	NULL       	0     								0
L16		0 		1 		0 		2700000 	2800000 	NULL       	0     								0
L17		0 		1 		0 		3000000 	3000000 	NULL       	0     								0
L18		0 		1 		0 		1200000 	1200000 	"8921_s4"  	0     								0
L23		1 		1 		0 		1800000 	1800000 	NULL       	0     								0
L24		0 		1 		1  		750000 		1150000 	"8921_s1" 	10000 								10000
L25		1 		1 		0 		1250000		1250000 	"8921_s1" 	10000 								10000
L27		0 		0 		0 		1100000 	1100000 	"8921_s7"  	0     								0
L28		0 		1 		0 		1050000 	1200000 	"8921_s7"  	0     								0
LVS1 	0 		1 		0                   			"8921_s4"
LVS3 	0 		1 		0                  				"8921_s4"
LVS4 	0 		1 		0                   			"8921_s4"
LVS5 	0 		1 		0                   			"8921_s4"
LVS6 	0 		1 		0                   			"8921_s4"
LVS7 	0 		1 		1                   			"8921_s4"
NCP		0    			0 		1800000 	1800000 	"8921_l6" 				1p60


// **** GPIO TOYS **** //
//ID      	vreg_name 		gpio_label   		gpio  						supply 		active_low
EXT_5V 		"ext_5v" 		"ext_5v_en" 		PM8921_MPP_PM_TO_SYS(7		NULL 		0
EXT_OTG_SW 	"ext_otg_sw" 	"ext_otg_sw_en"		PM8921_GPIO_PM_TO_SYS(42 	NULL 		1


// **** SAW TOYS **** // 		(These feed the krait cores on the qualcomm snapdragon SoC)
//ID  		vreg_name	       	min_uV   	max_uV */
S5 			"8921_s5"	       	850000 		1300000
S6 			"8921_s6"	       	850000 		1300000
8821_S0 	"8821_s0"       	850000 		1300000
8821_S1 	"8821_s1"       	850000 		1300000


// **** PM8921 Toys **** //
//ID 		name 				always_on 	pd 		min_uV   	max_uV   	en_t 	supply 			system_uA 	reg_ID
L26 		"8921_l26" 			0 			1 		375000 		1050000 	200 	"8921_s7" 		0 			1
L29      	"8921_l29" 			0 			1 		1800000 	1800000 	200 	NULL 			0 			4


// **** PM8917 TOYs **** //
//ID   		name 				always_on 	pd 		min_uV   	max_uV   	en_t 	supply 			system_uA 	reg_ID
L26 		"8921_l26" 			0 			1 		375000 		1050000 	200 	"8921_s7" 		0 			1
L30			"8917_l30" 			0 			1 		1800000		1800000 	200 	NULL  			0 			2
L31			"8917_l31" 			0 			1 		1800000		1800000 	200 	NULL 			0 			3
L32			"8917_l32" 			0 			1 		2800000		2800000 	200 	NULL 			0 			4
L33			"8917_l33" 			0 			1 		2800000		2800000 	200 	NULL 			0 			5
L34			"8917_l34" 			0 			1 		1800000		1800000 	200 	NULL 			0 			6
L35			"8917_l35" 			0 			1 		3000000		3000000 	200 	NULL 			0 			7
L36			"8917_l36" 			0 			1 		1800000		1800000 	200 	NULL 			0 			8
	
//ID     	name   				always_on	  		min_uV   	max_uV 		en_t 	supply 						reg_ID
BOOST		"8917_boost" 		0  					5000000 	5000000 	500 	NULL 						9

//ID        name      			always_on 	pd 								en_t 	supply    					reg_ID
USB_OTG 	"8921_usb_otg" 		0 			1 								0   	"8917_boost" 				10

//ID  							a_on 		pd 		ss                   			supply
LVS2 							0 			1 		0                   			"8921_s1"


=======================================================================================================================
=======================================================================================================================

Power Regulator Consumers
//			regulator name				consumer dev_name

//	L1 runs at: 1100000 / 1100000
// ** **
// ** ** m0nk: Very curious where this runs... gut says a power plane for random components... ?
// ** **
L1 =		"8921_l1"					NULL

//	L2 runs at: 1200000 / 1200000
// ** **
// ** ** m0nk: mipi_csi == camera interface
// ** ** m0nk: mipi_dsi == display interface
// ** ** m0nk: tabla2x -> I think this is sound card / sound support?
// ** ** ref: http://www.mipi.org/specifications/camera-interface
// ** **
L2 =		"8921_l2"					NULL
			"mipi_csi_vdd"				"msm_csid.0"
			"mipi_csi_vdd"				"msm_csid.1"
			"mipi_csi_vdd"				"msm_csid.2"
			"lvds_pll_vdda"				"lvds.0"
			"dsi1_pll_vdda"				"mipi_dsi.1"
			"HRD_VDDD_CDC_D"			"tabla2x-slim"
			"HRD_CDC_VDDA_A_1P2V"		"tabla2x-slim"
			"dsi_pll_vdda"				"mdp.0"

//	L3 runs at: 3075000 / 3075000
// ** **
// ** ** m0nk: This is the regulator for the USB hardware and OTG
// ** **
L3 =		"8921_l3"					NULL
			"HSUSB_3p3"					"msm_otg"
			"HSUSB_3p3"					"msm_ehci_host.0"
			"HSUSB_3p3"					"msm_ehci_host.1"

//	L4 runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: This is the regulator for the USB OTG Hardware 
// ** ** m0nk: This is the regulator for the wlan hardware
// ** **
L4 =		"8921_l4"					NULL
			"HSUSB_1p8"					"msm_otg"
			"iris_vddxo"				"wcnss_wlan.0"

//	L5 runs at: 2950000 / 2950000
// ** **
// ** ** m0nk: Google shows us that the msm_sdcc relates to the: Qualcomm MSM 7X00A SDCC
// ** ** 		This provides support for the SD/MMC cell found in the MSM and QSD SOCs from Qualcomm. 
// ** **			The controller also has support for SDIO devices.
// ** **
// ** ** ref: http://cateee.net/lkddb/web-lkddb/MMC_MSM.html 
// ** ** ref: http://lxr.free-electrons.com/source/drivers/mmc/host/msm_sdcc.c
// ** **
L5 = 		"8921_l5"					NULL
			"sdc_vdd"					"msm_sdcc.1"

//	L6 runs at: 2950000 / 2950000
// ** **
// ** ** m0nk: Google shows us that the msm_sdcc relates to the: Qualcomm MSM 7X00A SDCC
// ** ** 		This provides support for the SD/MMC cell found in the MSM and QSD SOCs from Qualcomm. 
// ** **			The controller also has support for SDIO devices.
// ** **
// ** ** ref: http://cateee.net/lkddb/web-lkddb/MMC_MSM.html 
// ** ** ref: http://lxr.free-electrons.com/source/drivers/mmc/host/msm_sdcc.c
// ** **
L6 = 		"8921_l6"					NULL
			"sdc_vdd"					"msm_sdcc.3"

//	L7 runs at: 1850000 / 2950000
// ** **
// ** ** m0nk: Google shows us that the msm_sdcc relates to the: Qualcomm MSM 7X00A SDCC
// ** ** 		This provides support for the SD/MMC cell found in the MSM and QSD SOCs from Qualcomm. 
// ** **			The controller also has support for SDIO devices.
// ** **
// ** ** ref: http://cateee.net/lkddb/web-lkddb/MMC_MSM.html 
// ** ** ref: http://lxr.free-electrons.com/source/drivers/mmc/host/msm_sdcc.c
// ** **
L7 =		"8921_l7"					NULL
			"sdc_vdd_io"				"msm_sdcc.3"

//	L8 runs at: 2800000 / 2800000
// ** **
// ** ** m0nk: I'm not a rocket scientist or anything, but I bet this is a camera
// ** **
L8 =		"8921_l8"					NULL
			"cam_vana"					"4-001a"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vana"					"4-0048"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vana"					"4-006c"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vana"					"4-0034"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vana"					"4-0020"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vana"					"4-0010"		//else
			"cam_vana"					"4-0036"		//else

//	L9 runs at: 2850000 / 2850000
// ** **
// ** ** m0nk: apds9702 is the tristate "APDS9702 proximity driver"
// ** ** m0nk: mpu3050  is the gyroscope
// ** ** m0nk: bma250   is the Bosch accelerometer
// ** ** m0nk: akm8963  is the compass
// ** **
// ** **
L9 =		"8921_l9"					NULL
			"vdd"						"3-0024"
			"apds9702_vdd"				"2-0054"
			"mpu3050_vdd"				"2-0068"
			"bma250_vdd"				"2-0018"
			"akm8963_vdd"				"2-000c"
			"irda_vcc"					"irda"

//	L10 runs at: 2900000 / 2900000
// ** **
// ** ** monk: wifi?
// ** **
L10 = 		"8921_l10"					NULL
			"iris_vddpa"				"wcnss_wlan.0"

//	L11 runs at: 2850000 / 2850000
// ** **
// ** ** m0nk: mipi_dsi == display interface
// ** ** m0nk: lm3533 -> drivers/leds/leds-lm3533_ng.c
// ** **
L11 = 		"8921_l11"					NULL
			"desi1_avdd"				"mipi_dsi.1"
			"lm3533_als"				"0-0036"

//	L12 runs at: 1200000 / 1200000
// ** **
// ** ** m0nk: again, $100 says it's a camera
// ** **
L12 = 		"8921_l12"					NULL
	 		"cam_vdig"					"4-001a"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vdig"					"4-0048"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vdig"					"4-006c"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vdig"					"4-0034"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vdig"					"4-0020"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vdig"					"4-0036"		//else

//	L13 runs at: 1740000 / 1740000
// ** **
// ** ** m0nk: therm sounds interesting... _adc does as well <-- poke hard here 
// ** **
L13 = 		"8921_l13"					NULL
	 		"apq_therm"					"pm8xxx-adc"

//	L14 runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: therm sounds interesting... _adc does as well, as does CHARGER <-- poke hard here 
// ** **
L14 = 		"8921_l14"					NULL
	 		"vreg_xoadc"				"pm8921-charger"
	 		"pa_therm"					"pm8xxx-adc"

//	L16 runs at: 2700000 / 2800000
// ** **
// ** ** m0nk: again, $100 says it's a camera
// ** **
L16 = 		"8921_l16"					NULL
	 		"cam_vaf"					"4-001a"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vaf"					"4-0048"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vaf"					"4-006c"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vaf"					"4-0034"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vaf"					"4-0010"		//else

//	L17 runs at: 3000000 / 3000000
// ** **
// ** ** m0nk: looks to be touch screen power.  could be interesting to kill or neuter this one :)
// ** **
L17 =		"8921_l17"					NULL
	 		"touch_vdd"					"3-002c"

//	L18 runs at: 1200000 / 1200000
// ** **
// ** ** m0nk: Very curious where this runs... gut says a power plane for random components... ?
// ** **
L18 = 		"8921_l18"					NULL

//	L23 runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: This is the regulator for the USB hardware
// ** ** m0nk: The qdsp6* stuff seems to be more audio stuff (../mach-msm/qdsp6
// ** ** m0nk: also references to Hexagon processors (see below)
// ** **
L23 = 		"8921_l23"					NULL
	 		"pll_vdd"					"pil_qdsp6v4.1"
	 		"pll_vdd"					"pil_qdsp6v4.2"
	 		"HSUSB_1p8"					"msm_ehci_host.0"
	 		"HSUSB_1p8"					"msm_ehci_host.1"
	 		"pn544_pvdd"				"0-0028"

//	L24 runs at: 750000 / 1150000
// ** **
// ** ** m0nk: WiFi?
// ** **
L24 = 		"8921_l24"					NULL
	 		"riva_vddmx"				"wcnss_wlan.0"

//	L25 runs at: 1250000 / 1250000
// ** **
// ** ** m0nk: Not really SURE here... drivers/mfd/wcd9xxx-core.c has a ton of references?
// ** ** m0nk: tabla2x -> I think this is sound card / sound support?
// ** **
L25 = 		"8921_l25"					NULL
	 		"VDDD_CDC_D"				"tabla-slim"
	 		"CDC_VDDA_A_1P2V"			"tabla-slim"
	 		"VDDD_CDC_D"				"tabla2x-slim"
	 		"CDC_VDDA_A_1P2V"			"tabla2x-slim"

//	L26 runs at: 375000 / 1050000
// ** **
// ** ** m0nk: ??? -> tristate "QDSP6v4 (Hexagon) Boot Support"
// ** ** ...          Support for booting and shutting down QDSP6v4 processors (hexagon).
// ** **
L26 = 		"8921_l26"					NULL
	 		"core_vdd"					"pil_qdsp6v4.0"

//	L27 runs at: 1100000 / 1100000
// ** **
// ** ** m0nk: ??? -> tristate "QDSP6v4 (Hexagon) Boot Support"
// ** ** ...          Support for booting and shutting down QDSP6v4 processors (hexagon).
// ** **
L27 = 		"8921_l27"					NULL
	 		"core_vdd"					"pil_qdsp6v4.2"

//	L28 runs at: 1050000 / 1200000
// ** **
// ** ** m0nk: ??? -> tristate "QDSP6v4 (Hexagon) Boot Support"
// ** ** ...          Support for booting and shutting down QDSP6v4 processors (hexagon).
// ** **
L28 = 		"8921_l28"					NULL
	 		"core_vdd"					"pil_qdsp6v4.1"
	 		"cam_vdig"					"4-0010"		//#if defined(CONFIG_SONY_CAM_V4L2)

//	L29 runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: mipi_dsi == display interface
// ** **
L29 =		"8921_l29"					NULL
	 		"dsi1_vddio"				"mipi_dsi.1"

//	S2 runs at: 1300000 / 1300000
// ** **
// ** ** m0nk: WiFi?
// ** **
S2 = 		"8921_s2"					NULL
	 		"iris_vddrfa"				"wcnss_wlan.0"

//	S3 runs at: 500000 / 1150000
// ** **
// ** ** m0nk: This is the regulator for the USB hardware and OTG
// ** **
S3 = 		"8921_s3"					NULL
	 		"HSUSB_VDDCX"				"msm_otg"
	 		"HSUSB_VDDCX"				"msm_ehci_host.0"
	 		"HSUSB_VDDCX"				"msm_ehci_host.1"
	 		"HSIC_VDDCX"				"msm_hsic_host"
	 		"riva_vddcx"				"wcnss_wlan.0"
	 		"vp_pcie"          		   	"msm_pcie"
	 		"vptx_pcie"        		   	"msm_pcie"

//	S4 runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: Google shows us that the msm_sdcc relates to the: Qualcomm MSM 7X00A SDCC
// ** ** 		This provides support for the SD/MMC cell found in the MSM and QSD SOCs from Qualcomm. 
// ** **			The controller also has support for SDIO devices.
// ** **
// ** ** ref: http://cateee.net/lkddb/web-lkddb/MMC_MSM.html 
// ** ** ref: http://lxr.free-electrons.com/source/drivers/mmc/host/msm_sdcc.c
// ** **
S4 = 		"8921_s4"					NULL
	 		"sdc_vdd_io"				"msm_sdcc.1"
	 		"VDDIO_CDC"					"tabla-slim"
	 		"CDC_VDD_CP"				"tabla-slim"
	 		"CDC_VDDA_TX"				"tabla-slim"
	 		"CDC_VDDA_RX"				"tabla-slim"
	 		"VDDIO_CDC"					"tabla2x-slim"
	 		"CDC_VDD_CP"				"tabla2x-slim"
	 		"CDC_VDDA_TX"				"tabla2x-slim"
	 		"CDC_VDDA_RX"				"tabla2x-slim"
	 		"riva_vddpx"				"wcnss_wlan.0"
	 		"vcc_i2c"					"3-005b"
	 		"vcc_i2c"					"3-0024"
			"vddp"						"0-0048"
	 		"hdmi_lvl_tsl"				"hdmi_msm.0"
	 		"touch_vio"					"3-002c"

//	S5 runs at: 850000 / 1300000
// ** **
// ** ** m0nk: This is the regulator for the Snapdragon: krait core 0
// ** **
S5 = 		"8921_s5"					NULL
	 		"krait0"					"acpuclk-8064"

//	S6 runs at: 850000 / 1300000
// ** **
// ** ** m0nk: This is the regulator for the Snapdragon: krait core 1
// ** **
S6 = 		"8921_s6"					NULL
	 		"krait1"					"acpuclk-8064"

//	S7 runs at: 1300000 / 1300000
// ** **
// ** ** m0nk: Very curious where this runs... gut says a power plane for random components... ?
// ** **
S7 = 		"8921_s7"					NULL

//	LVS1 runs at: ??? / ???
// ** **
// ** ** m0nk: WiFi?
// ** **
LVS1 = 		"8921_lvs1"					NULL
	 		"iris_vddio"				"wcnss_wlan.0"

//	LVS3 runs at: ??? / ???
// ** **
// ** **
// ** **
LVS3 = 		"8921_lvs3"					NULL

//	LVS4 runs at: ??? / ???
// ** **
// ** **
// ** **
LVS4 = 		"8921_lvs4"					NULL
	 		"apds9702_vio"				"2-0054"
	 		"mpu3050_vio"				"2-0068"
	 		"bma250_vio"				"2-0018"
	 		"akm8963_vio"				"2-000c"
	 		"irda_vio"					"irda"

//	LVS5 runs at: ??? / ???
// ** **
// ** ** m0nk: again, $100 says it's a camera
// ** **
LVS5 = 		"8921_lvs5"					NULL
			"cam_vio"					"4-001a"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vio"					"4-0048"		//#if !defined(CONFIG_SONY_CAM_V4L2)
			"cam_vio"					"4-006c"		//#if !defined(CONFIG_SONY_CAM_V4L2)
	 		"cam_vio"					"4-0010"		//#else
	 		"cam_vio"					"4-0036"		//#else
	 		"cam_vio"					"4-0034"		//#if !defined(CONFIG_SONY_CAM_V4L2)

//	LVS6 runs at: ??? / ???
// ** **
// ** **
// ** **
LVS6 = 		"8921_lvs6"					NULL
	 		"vdd_pcie_vph"        		"msm_pcie"

//	LVS7 runs at: ??? / ???
// ** **
// ** **
// ** **
LVS7 = 		"8921_lvs7"					NULL
	 		"pll_vdd"					"pil_riva"
	 		"lvds_vdda"					"lvds.0"
	 		"dsi_pll_vddio"				"mdp.0"
	 		"hdmi_vdda"					"hdmi_msm.0"

//	USB_OTG runs at: ??? / ???
// ** **
// ** ** m0nk: USB OTG here!
// ** **
USB_OTG = 	"8921_usb_otg"				NULL
	 		"vbus_otg"					"msm_otg"

//	8821_S0 runs at: 850000 / 1300000
// ** **
// ** ** m0nk: This is the regulator for the Snapdragon: krait core 2
// ** **
8821_S0 = 	"8821_s0"					NULL
	 		"krait2"					"acpuclk-8064"

//	8821_S1 runs at: 850000 / 1300000
// ** **
// ** ** m0nk: This is the regulator for the Snapdragon: krait core 3
// ** **
8821_S1 = 	"8821_s1"					NULL
	 		"krait3"					"acpuclk-8064"

//	S1 runs at: 1225000 / 1225000
// ** **
// ** ** m0nk: Very curious where this runs... gut says a power plane for random components... ?
// ** **
S1 = 		"8921_s1"					NULL

//	LVS2 runs at: ??? / ???
// ** **
// ** ** m0nk: WiFi?
// ** **
LVS2 = 		"8921_lvs2"					NULL
			"iris_vdddig"				"wcnss_wlan.0"

//	NCP runs at: 1800000 / 1800000
// ** **
// ** ** m0nk: Very curious where this runs... gut says a power plane for random components... ?
// ** **
NCP = 		"8921_ncp"					NULL

//	EXT_5V runs at: ??? / ???
EXT_5V = 	"ext_5v"					NULL

//	EXT_OTG_SW runs at: ??? / ???
EXT_OTG_SW = "ext_otg_sw"				NULL



/* Regulators that are only present when using PM8917 */

// ** **
// ** ** m0nk: WiFi?
// ** **
8917_S1 = 	"8921_s1"					NULL
	 		"iris_vdddig"				"wcnss_wlan.0"

L30 = 		"8917_l30"					NULL

L31 = 		"8917_l31"					NULL

L32 = 		"8917_l32"					NULL

L33 = 		"8917_l33"					NULL

L34 = 		"8917_l34"					NULL

L35 = 		"8917_l35"					NULL

L36 = 		"8917_l36"					NULL

BOOST = 	"8917_boost"				NULL
	 		"vbus"						"msm_ehci_host.0"
	 		"hdmi_mvs"					"hdmi_msm.0"


=======================================================================================================================
=======================================================================================================================


// rpm_regulator_consumer_mapping init !!!
	LVS7 	0 		1 		"krait0_hfpll" 	"acpuclk-8064"
	LVS7 	0 		2 		"krait1_hfpll" 	"acpuclk-8064"
	LVS7 	0 		4 		"krait2_hfpll" 	"acpuclk-8064"
	LVS7 	0 		5 		"krait3_hfpll" 	"acpuclk-8064"
	LVS7 	0 		6 		"l2_hfpll"     	"acpuclk-8064"
	L24  	0 		1 		"krait0_mem"   	"acpuclk-8064"
	L24  	0 		2 		"krait1_mem"   	"acpuclk-8064"
	L24  	0 		4 		"krait2_mem"   	"acpuclk-8064"
	L24  	0 		5 		"krait3_mem"   	"acpuclk-8064"
	S3   	0 		1 		"krait0_dig"   	"acpuclk-8064"
	S3   	0 		2 		"krait1_dig"   	"acpuclk-8064"
	S3   	0 		4 		"krait2_dig"   	"acpuclk-8064"
	S3   	0 		5 		"krait3_dig"   	"acpuclk-8064"


